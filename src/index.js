import {  canvas, ctx } from "./canvas"
import Line from "./class/Line"

ctx.strokeStyle = 'magenta'



const lineArr = []
const numberOfLines = 100

for (let i = 0; i < numberOfLines ; i++){
    lineArr.push(new Line())
}


function animate(){
    ctx.clearRect(0,0,canvas.clientWidth, canvas.height)
    lineArr.forEach(line=>line.update())
    requestAnimationFrame(animate)
}

animate()